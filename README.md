# Weight and balance

Weight and balance system using micropython on ESP32

![](ESP32-Pinout.jpg)


## Installation

## Put the libraries on the board : 

sudo ampy --port /dev/ttyUSB0 put ssd1306.py

sudo ampy --port /dev/ttyUSB0 put Adafruit_Thermal.py

sudo ampy --port /dev/ttyUSB0 put sdcard.py


 PINS

### DO NOT USE PIN D12 (it causes the error "rst:0x10 (RTCWDT_RTC_RESET),boot:0x33 (SPI_FAST_FLASH_BOOT) flash read err, 1000")

# Clock (I2C connection)
PIN_DS1307_SCL = 22
PIN_DS1307_SDA = 23

# Oled screen (I2C connection) /!\ do not put another device on this bus !
PIN_SSD1306_SCL = 13
PIN_SSD1306_SDA = 14

# Sd card reader (SPI connection)
PIN_SD_CS = 15
PIN_SD_SCK = 18  # conflict
PIN_SD_MOSI = 23 # conflict
PIN_SD_MISO = 19 # conflict

# Inclinometer
PIN_TXD = 26
PIN_RXD  = 27 (needed ?)

# Printer
PIN_RXD = ??

# Rotary encoder (DEPRECIATED)
PIN_CJMCU_11_GA = 35
PIN_CJMCU_11_GB = 34

# HX711 connections

| HX711 | DYLF-102    |
|-------|-------------|
| E-    | White wire  |
| E+    | Grey wire   |
| S-    | Purple wire |
| S+    | Blue wire   |

| HX711 TTL | ESP32                |
|-----------|----------------------|
| GND       | no connection needed |
| TXD       | 16, 17, 18, 19       |   
| RXD       | no connection needed |
| VCC       | no connection needed |

# HX711 Parameters - Buttons Hardware

Under Leds 4 buttons (from left):
no 1 = OK and Menu Preferences (press 3s to enter parameters)
no 2 = go left
no 3 = + and Peeling Low (press 3s -> -PL- : without any load)
no 4 = - and Peeling hight (press 3s -> -PH- : max 100kg with the exact load)

Parameters 
dot = dot before last led (=to shown one decimal)
ad-H = 100 (= 100 measures per second)
ts = 0.1 (send 10 measures per second and also when no change)
En-b = OFF (no Tare) 
ini = OFF (no reset factory defaults)

Peeling procedure (zero)
1 Take away any load
2 press button no 4 (-) for 3 seconds
3 - PL - appear (Peeling Low)
4 with buttons no 3 (+) and no 4 (-) show 000.0
5 press button no 1 : OK

Peeling procedure Hight (ie 100kg = Max possible)
1 put 100kg on the weight
2 press button no 3 (+) for 3 seconds
3 -PH- appear (Peeling high)
4 with buttons no 3 and no 4 show 100.0
5 press button no 1 : OK


# ESP32 Micropython quick ref

https://docs.micropython.org/en/latest/esp32/quickref.html#timers

## Project documentation

"test_print.py" is not directly used but provides some useful examples.

The "image_processing" folder contains the script and images used to generate the code used for the animated image.

## Credits

- MicroPython project
<https://github.com/micropython/micropython>

- "Adafruit_Thermal.py" library
<https://github.com/ayoy/micropython-thermal-printer>

- ssd1306 driver
<https://github.com/micropython/micropython/blob/master/drivers/display/ssd1306.py>

- MicroPython TinyRTC I2C Module, DS1307 RTC + AT24C32N EEPROM
<https://github.com/mcauser/micropython-tinyrtc>

- sdcard driver
<https://github.com/micropython/micropython/blob/master/drivers/sdcard/sdcard.py>