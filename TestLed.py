import machine
import time

blueLed = machine.Pin(2, machine.Pin.OUT)

def main():

    for i in range(3):

        blueLed.on()
        time.sleep(1)
        blueLed.off()
        time.sleep(1)

main()