import imageio

print("heli_frame_buffers = [")

def do_process(frame):
    im = imageio.imread("test_image/"+frame+".png")
    #print(im)

    str_bits = ""

    for j in im:
        for a in j:
            if a[3] != 0:
                str_bits += "1"
            else:
                str_bits += "0"

    n = 8
    output = [str_bits[i:i+n] for i in range(0, len(str_bits), n)]

    #framebuf.FrameBuffer(bytearray(heli_top_view_80_32),80,32,framebuf.MONO_HLSB)

    f = "framebuf.FrameBuffer(bytearray(["
    for o in output:
        f += str(int(o, 2))+","

    f = f[:-1]
    f += "]),80,32,framebuf.MONO_HLSB),\n"
    print(f)

do_process("0")

print("]")