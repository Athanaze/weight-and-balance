import imageio
import sys

THRESHOLD = 100

def do_process(name):
    im = imageio.imread(name)
    #print(im)

    str_bits = ""

    for j in im:
        for a in j:
            if a[3] > THRESHOLD:
                str_bits += "1"
            else:
                str_bits += "0"

    n = 8
    output = [str_bits[i:i+n] for i in range(0, len(str_bits), n)]

    f = "framebuf.FrameBuffer(bytearray(["
    for o in output:
        f += str(int(o, 2))+","

    f = f[:-1]
    f += "]),24,32,framebuf.MONO_HLSB)\n"
    print(f)

do_process(sys.argv[1])
