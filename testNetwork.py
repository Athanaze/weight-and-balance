import network

# Run WIFI Access point
ap = network.WLAN(network.AP_IF)    # create access-point interface
ap.config(essid='Weight&Balance')   # set the ESSID of the access point
ap.config(max_clients=4)            # set how many clients can connect to the network
ap.active(True)                     # activate the interface

# Run webpage delivery
import pageWeb.py
