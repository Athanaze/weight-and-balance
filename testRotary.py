# Simple testfile for the rotary encoder

from machine import Pin, I2C, UART
import ssd1306
import framebuf
from time import sleep
import math

from rotary_irq_esp import RotaryIRQ

sleep(4)
print('testRotary with rotary.py is running ...')


# Rotary Encoder
# clk = GPIO 22
#  dt = GPIO 23
# push= GPIO 21

pButton = Pin(21, Pin.IN) 

rButton = RotaryIRQ(pin_num_clk=22, 
              pin_num_dt=23, 
              min_val=48,              # @ in ASCII will be use for '-'
              max_val=90,              # Z in ASCII
              reverse=True, 
              range_mode=RotaryIRQ.RANGE_WRAP)
              
val_old = 0

# OLED screen all black
i2c = I2C(-1, scl=Pin(13), sda=Pin(14))

oled_width = 128
oled_height = 64

oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

oled.fill(0)
oled.show()

# titre
oled.text('    Settings    ',0,0,1)
oled.text('                ',0,10,1)
oled.text(' Immat:         ',0,20,1)
oled.text('                ',0,30,1)
oled.text(' Ref  :         ',0,40,1)
oled.text('                ',0,50,1)

oled.show()

# Immatriculation tab of 6 letters
immat = ['-','-','-','-','-','-']

for i in range(0,6):
    oled.text(immat[i],56+i*8,20,1)
    oled.show()

# input Immatriculation 
push = -10
save = 0
i = 0
while i < 6:

    if immat[i] == '-':
        rButton.set(64)
    else:
        rButton.set(ord(immat[i]))

    oled.fill_rect(56+i*8,28,8,1,1)
    oled.text(immat[i],56+i*8,20,1)
    oled.show()

    next = 0 # next letter

    while next == 0:

        if pButton.value() == 0:
            push +=1
        else:
            push = 0

        val_new = rButton.value()
        if val_old != val_new:
            push = 0
            val_old = val_new
            if val_new != 64:
                c = chr(val_new)
            else:
                c = '-'
            oled.fill_rect(56+i*8, 20, 8, 8, 0)
            oled.text(c,56+i*8,20,1)
            oled.show()
            sleep(0.2)

        sleep(0.2)

        if push > 15 & pButton.value() == 0:
            push = -20
            oled.fill_rect(56+i*8,28,8,1,0)
            oled.fill_rect(56+8+i*8,28,8,1,1)
            oled.show()
            while pButton.value() == 0:
                sleep(0.1)           
            next = 1
         
    immat[i] = c
    i += 1
    if i == 6 : 
        
        saveRedo = [
            framebuf.FrameBuffer(bytearray([16,48,124,54,18,6,124,0]),8,8,framebuf.MONO_HLSB),
            framebuf.FrameBuffer(bytearray([1,3,70,108,56,56,16,0]),8,8,framebuf.MONO_HLSB)
        ]
        
        while push < 12:
            oled.blit(saveRedo[int(math.fmod(rButton.value(),2))],104,20)
            oled.show()
            if pButton.value() == 0:
                push += 1
            else:
                push = 0
            sleep(0.1)

        oled.fill_rect(104,20,8,9,0)
        oled.show()

        if math.fmod(rButton.value(),2) == 1:
            save = 1  
        else:
            save = 0
            i = 0

print(immat)



