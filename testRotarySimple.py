# Simple testfile for the rotary encoder

from machine import Pin
from time import sleep

sleep(4)
print('testRotary is running ...')

pin_x = Pin(22, Pin.IN, Pin.PULL_UP)
pin_y = Pin(23, Pin.IN, Pin.PULL_UP)



class Encoder(object):
    def __init__(self, pin_x, pin_y, reverse, scale):
        self.reverse = reverse
        self.scale = scale
        self.forward = True
        self.pin_x = pin_x
        self.pin_y = pin_y
        self._pos = 0
        self.x_interrupt = pin_x.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=self.x_callback)
        self.y_interrupt = pin_y.irq(trigger=Pin.IRQ_RISING | Pin.IRQ_FALLING, handler=self.y_callback)

    def x_callback(self, line):
        self.forward = self.pin_x.value() ^ self.pin_y.value() ^ self.reverse
        self._pos += 1 if self.forward else -1
    
    def y_callback(self, line):
        self.forward = self.pin_x.value() ^ self.pin_y.value() ^ self.reverse ^ 1
        self._pos += 1 if self.forward else -1

    @property
    def position(self):
        return self._pos * self.scale

    @position.setter
    def position(self, value):
        self._pos = value // self.scale

# encoder = Encoder(pin_x,pin_y,0,1)

# while True:
#    print(encoder.position)
#    sleep(0.1)

counter = 100
aLastState = 0


def caBouge(pin):

    global counter
    
    if pin_y.value() != pin_x.value():
        counter += 1
    else:
        counter -=1

    print('caBouge : '+str(counter))
    sleep(0.1)

interrupt = pin_x.irq(trigger=Pin.IRQ_RISING, handler=caBouge)

while True:

    # print(counter)
    sleep(0.2)
