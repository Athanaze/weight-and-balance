from machine import Pin, I2C, UART
import time
import ssd1306
import framebuf
from Adafruit_Thermal import *
import math

from machine import ADC

# initialise ADC captors
adcCollective = ADC(Pin(36))         
adcThrottle = ADC(Pin(39))       

adcCollective.atten(ADC.ATTN_11DB)    # set 11dB input attenuation (voltage range roughly 0.0v - 3.6v)
adcThrottle.atten(ADC.ATTN_11DB)    # set 11dB input attenuation (voltage range roughly 0.0v - 3.6v)

# adc.width(ADC.WIDTH_9BIT)   # set 9 bit return values (returned range 0-511)

maxC = 0
minC = 4096

maxT = 0
minT = 4096

while True :
    
    collective = 0
    throttle = 0

    for i in range(1, 100) :
        collective += adcCollective.read()
        throttle += adcThrottle.read()
        time.sleep(0.1)
    
    collective /= 100
    throttle /= 100

    # collective = round((collective-1750)/3750*90,1)
    collective = round(collective,0)
    throttle = round(throttle,0)   

    
    if collective > maxC : maxC = collective
    if collective < minC : minC = collective
    if throttle > maxT : maxT = throttle
    if throttle < minT : minT = throttle


    print(str(collective)+' max C = '+str(maxC)+' min C = '+str(minC)+' Throttle : '+str(throttle)+' max C = '+str(maxT)+' min C = '+str(minT))


    time.sleep(0.1)

