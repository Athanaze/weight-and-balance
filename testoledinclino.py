from machine import Pin, I2C, UART, ADC
import time
import ssd1306
import framebuf
from Adafruit_Thermal import *
from time import sleep
import math
import network
import socket

import gc
gc.collect()

sleep(4)

print("testoled.py is running ...")

# Setting variables
pd1 = 0
pd2 = 0
pd3 = 0
pd4 = 0

pdl = 0
pdr = 0
pdft = 0
pdre = 0
pdtot = 0

posRac = 0

dt = 0 # dateTime human readable
tt = 0 # time Tick in ms

recordLetter = 'A'
lastRecordLetter = '@'
recordOn = False        # Flag

collective = 0
throttle = 0

# Create the recordLetter file if doesn't exist
try:
    f = open('recordLetter.txt', 'r')
    lastRecordLetter = f.read()
    if lastRecordLetter == '' : lastRecordLetter = '@'
    n = ord(lastRecordLetter)+1
    if n >90 : n = 65
    recordLetter = chr(n)
    f.close()
    
except OSError:
    f = open('recordLetter.txt','w')
    f.write(lastRecordLetter)
    recordLetter = 'A'
    f.close()

# Clock (I2C connection) ###################### Clock ##############3
PIN_DS1307_SCL = 22         # Pin22
PIN_DS1307_SDA = 23         # Pin23

DATETIME_REG = const(0) # 0x00-0x06
CHIP_HALT    = const(128)
CONTROL_REG  = const(7) # 0x07
RAM_REG      = const(8) # 0x08-0x3F

def make2digitsStr(intValue):
    baseStr = "0"
    if intValue > 9:
        baseStr = str(intValue)
    else:
        baseStr += str(intValue)

    return baseStr


class DS1307(object):
    """Driver for the DS1307 RTC."""
    def __init__(self, i2c, addr=0x68):
        self.i2c = i2c
        self.addr = addr
        self.weekday_start = 1
        self._halt = False

    def _dec2bcd(self, value):
        """Convert decimal to binary coded decimal (BCD) format"""
        return (value // 10) << 4 | (value % 10)

    def _bcd2dec(self, value):
        """Convert binary coded decimal (BCD) format to decimal"""
        return ((value >> 4) * 10) + (value & 0x0F)

    def datetime(self, datetime=None):
        """Get or set datetime"""
        if datetime is None:
            buf = self.i2c.readfrom_mem(self.addr, DATETIME_REG, 7)
            return (
                self._bcd2dec(buf[6]) + 2000, # year
                self._bcd2dec(buf[5]), # month
                self._bcd2dec(buf[4]), # day
                self._bcd2dec(buf[3] - self.weekday_start), # weekday
                self._bcd2dec(buf[2]), # hour
                self._bcd2dec(buf[1]), # minute
                self._bcd2dec(buf[0] & 0x7F), # second
                0 # subseconds
            )
        buf = bytearray(7)
        buf[0] = self._dec2bcd(datetime[6]) & 0x7F # second, msb = CH, 1=halt, 0=go
        buf[1] = self._dec2bcd(datetime[5]) # minute
        buf[2] = self._dec2bcd(datetime[4]) # hour
        buf[3] = self._dec2bcd(datetime[3] + self.weekday_start) # weekday
        buf[4] = self._dec2bcd(datetime[2]) # day
        buf[5] = self._dec2bcd(datetime[1]) # month
        buf[6] = self._dec2bcd(datetime[0] - 2000) # year
        if (self._halt):
            buf[0] |= (1 << 7)
        self.i2c.writeto_mem(self.addr, DATETIME_REG, buf)

    def halt(self, val=None):
        """Power up, power down or check status"""
        if val is None:
            return self._halt
        reg = self.i2c.readfrom_mem(self.addr, DATETIME_REG, 1)[0]
        if val:
            reg |= CHIP_HALT
        else:
            reg &= ~CHIP_HALT
        self._halt = bool(val)
        self.i2c.writeto_mem(self.addr, DATETIME_REG, bytearray([reg]))

    def square_wave(self, sqw=0, out=0):
        """Output square wave on pin SQ at 1Hz, 4.096kHz, 8.192kHz or 32.768kHz,
        or disable the oscillator and output logic level high/low."""
        rs0 = 1 if sqw == 4 or sqw == 32 else 0
        rs1 = 1 if sqw == 8 or sqw == 32 else 0
        out = 1 if out > 0 else 0
        sqw = 1 if sqw > 0 else 0
        reg = rs0 | rs1 << 1 | sqw << 4 | out << 7
        self.i2c.writeto_mem(self.addr, CONTROL_REG, bytearray([reg]))
    
    # Returns a the date and time in a string formatted like so : DD/MM/YYYY HH:MM
    
    def formattedDateTimeStr(self):
        now = self.datetime()
        formattedDateTime = make2digitsStr(now[2])+"/"+make2digitsStr(now[1])+"/"+str(now[0])+" "+make2digitsStr(now[4])+":"+make2digitsStr(now[5])+"."+make2digitsStr(now[6])

        return formattedDateTime

    def formattedDateTimeStrBis(self):
        now = self.datetime()
        formattedDateTime = make2digitsStr(now[0])+make2digitsStr(now[1])+str(now[2])+make2digitsStr(now[4])+make2digitsStr(now[5])+make2digitsStr(now[6])

        return formattedDateTime

    # Returns the current time in a fixed-size array (example : [0,9,1,2] for 09:12)
    def time_as_array(self):
        now = self.datetime()
        hh = make2digitsStr(now[4])
        mm = make2digitsStr(now[5])

        return [hh[0], hh[1], mm[0], mm[1]]

# Instanciate clock
ds = DS1307(I2C(freq=400000, scl=Pin(PIN_DS1307_SCL), sda=Pin(PIN_DS1307_SDA)))
ds.halt(False)
# now = (2020, 11, 25, 3, 13, 34, 0, 0)     # uncomment to fix date
# ds.datetime(now)
print('Time UTC RTC: '+ds.formattedDateTimeStr())


# Run WIFI Access point ####################################################### WIFI & WEB #################
ap = network.WLAN(network.AP_IF)    # create access-point interface
ap.config(essid='Weight&Balance')   # set the ESSID of the access point
ap.config(max_clients=4)            # set how many clients can connect to the network
ap.active(True)                     # activate the interface

# WEB Server
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)
s.settimeout(0) # nonblocking mode

led = Pin(2, Pin.OUT) # just for fun and demo

def web_page(webPageType):
    
    html = ''

    if webPageType == 0 :       # Base page

        if led.value() == 1:
            gpio_state="ON"
        else:
            gpio_state="OFF"

        logButtonList = ''
        for i in range(65,90):
            logButtonList += """<a href="/W&B_Log_"""+chr(i)+"""_"""+dtbis+""".csv?data=3"""+chr(i)+""""><input type="button" value = 'Log """+chr(i)+"""' " /></a>"""
        
        html = """<html><head> <title>Weight & Balance</title> <meta http-equiv="refresh" content="1"><meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="data:,"><style>html{font-family: Helvetica; display:inline-block; margin: 20px auto; text-align: right;}
        h1{color: #0F3376; padding: 2vh;}
        p{font-size: 1rem;}
        .button{display: inline-block; background-color: #e7bd3b; border: none; border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
        .button2{background-color: #4286f4;}</style></head>
        <body> <h1>Weight & Balance</h1>
        <p align='center'>Log :"""+str(recordLetter)+""" """+str(dt)+"""</p> 
        <p>Poids Rear Left  : """+str(pd1/10)+"""</p>
        <p>Poids Front Left : """+str(pd2/10)+"""</p>
        <p>Poids Front Right: """+str(pd3/10)+"""</p>
        <p>Poids Rear Right : """+str(pd4/10)+"""</p>
        <p><b>Poids Total      : """+str(pdtot/10)+"""</b></p>
        <p></p>
        <p>Collective : """+str(collective)+"""deg</p>
        <p>Throttle : """+str(throttle)+"""</p>
        <p></p>
        <input type="button" value = "Rafraîchir" onclick="history.go(0)" />
        <p></p>
        <a href="/?data=1"><input type="button" value = 'Current data' " /></a>
        """+logButtonList+"""               
        <p></p>
        <p>GPIO state: <strong>""" + gpio_state + """</strong></p><p><a href="/?led=on"><button class="button">ON</button></a></p>
        <p><a href="/?led=off"><button class="button button2">OFF</button></a></p>
        
        </body></html>"""

    if webPageType == 1 :       # Only current data

        html = recordLetter+","+str(dt)+','+str(tt)+','+str(pd1/10)+","+str(pd2/10)+","+str(pd3/10)+","+str(pd4/10)+","+str(collective)+","+str(throttle)

    if webPageType == 2 :       # data.txt

        f = open('data.txt','r')
        html = str(f.read())

    if webPageType == 3 :       # graph

        html += 'recordLetter,time,ticks,RL,FL,FR,RR,collective,throttle\n'
        with open('data.txt') as f:
            for line in f:
                if line[0] == request[15]:
                    print(str(line))
                    html += str(line)


    return html

def web_service():
    
    try:
        
        global webPageType 
        global request

        webPageType = 0
        request = 0 

        conn, addr = s.accept()
        print('Got a connection from %s' % str(addr))
        request = conn.recv(1024)
        request = str(request)
        print('Request = %s' % request)
        led_on = request.find('/?led=on')
        led_off = request.find('/?led=off')
        data1 = request.find('/?data=1')                # only data, no layout
        data2 = request.find('/W&B.csv?data=2')         # data.txt en csv
        data3 = request.find('?data=3')                 # log Letter

        if led_on == 6:
            print('LED ON')
            led.value(1)
        if led_off == 6:
            print('LED OFF')
            led.value(0)
        
        if data1 == 6:
            print('Data 1')
            webPageType = 1
        if data2 == 6:
            print('Data 2')
            webPageType = 2
        if data3 == 35:
            print('Data 3')
            webPageType = 3
       
        
        response = web_page(webPageType)
        conn.send('HTTP/1.1 200 OK\n')
        if webPageType == 0 : conn.send('Content-Type: text/html\n')
        if webPageType == 1 : conn.send('Content-Type: text/html\n')
        if webPageType == 2 : conn.send('Content-Type: text/csv\n')
        if webPageType == 3 : conn.send('Content-Type: text/csv\n')
        conn.send('Connection: close\n\n')
        conn.sendall(response)
        conn.close()

        return True

    except OSError:

        print('no web request')
        return False

# Connection to OLED SCREEN - 2.42' OLED 
i2c = I2C(-1, scl=Pin(13), sda=Pin(14))                         # PIN 13 et 14 = OLED screen

oled_width = 128
oled_height = 64

oled = ssd1306.SSD1306_I2C(oled_width, oled_height, i2c)

# Pin configuration : Button
button1 = Pin(32, Pin.IN, Pin.PULL_UP)     # Pin(33, Pin.IN, Pin.PULL_UP) # enable internal pull-up resistor
print(button1.value())       
button2 = Pin(33, Pin.IN, Pin.PULL_UP)     
print(button1.value())       

# initialise ADC sensors for collective and throttle
adcCollective = ADC(Pin(36))         
adcThrottle = ADC(Pin(39))       

adcCollective.atten(ADC.ATTN_11DB)    # set 11dB input attenuation (voltage range roughly 0.0v - 3.6v)
adcThrottle.atten(ADC.ATTN_11DB)    # set 11dB input attenuation (voltage range roughly 0.0v - 3.6v)

# DEPRECIATED : now connection UART in Measurement loop (b.o. only 3 UART on ESP32)
# Connection to weight sensors - hx711 with LED
# uart1 = UART(1, 9600)
# uart1.init(baudrate=9600, bits=8, parity=None, stop=1, rx=35)   # PIN 35 = Weight sensor 1 rear-left
# uart2 = UART(2, 9600)
# uart2.init(baudrate=9600, bits=8, parity=None, stop=1, rx=34)   # PIN 34 = Weight sensor 2 front-left
# uart3 = UART(3, 9600)
# uart3.init(baudrate=9600, bits=8, parity=None, stop=1, rx=39)   # PIN 39 = Weight sensor 3 front-right
# uart4 = UART(4, 9600)
# uart4.init(baudrate=9600, bits=8, parity=None, stop=1, rx=36)   # PIN 36 = Weight sensor 4 rear-right

# Rotorway en coupe
fb = framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,7,240,0,0,0,0,0,0,31,0,0,15,255,128,0,0,0,255,248,0,0,0,0,127,255,255,255,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,96,0,0,0,0,63,254,0,0,0,32,0,0,0,0,127,255,128,0,0,48,128,0,0,0,255,231,240,0,0,49,128,0,0,3,255,207,252,0,0,27,0,0,0,15,255,248,31,0,0,30,28,0,0,63,255,240,7,128,0,28,14,0,0,255,255,224,3,192,0,31,255,255,255,255,255,224,1,224,0,63,255,255,255,255,255,224,0,224,0,127,255,255,255,255,255,224,0,112,0,220,0,15,255,254,31,240,0,112,0,152,0,0,0,255,31,240,0,112,0,16,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB)

# Chiffre 1  - TTF b612 24 32

ch = [
    framebuf.FrameBuffer(bytearray([0,0,0,0,24,0,1,255,128,3,255,192,7,255,240,15,255,248,31,255,248,63,255,252,63,195,252,63,193,254,127,129,254,127,129,254,127,128,255,127,128,255,255,128,255,255,0,255,255,0,255,255,0,255,255,0,255,255,128,255,255,128,255,127,128,255,127,128,255,127,128,255,127,129,254,63,193,254,63,195,254,31,231,252,31,255,252,15,255,248,7,255,240,3,255,224]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,0,14,0,0,31,192,0,63,192,0,127,192,0,255,192,1,255,192,7,255,192,15,255,192,31,255,192,63,255,192,31,223,192,31,159,192,15,31,192,6,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,0,63,192,7,255,254,7,255,254,7,255,254,7,255,254]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,28,0,1,255,192,7,255,224,15,255,248,31,255,248,31,255,252,63,195,252,63,129,254,31,129,254,7,1,254,0,1,254,0,1,254,0,3,254,0,3,252,0,7,252,0,15,248,0,31,240,0,63,224,0,127,224,0,255,192,1,255,128,3,255,0,3,254,0,7,252,0,15,248,0,31,248,0,31,240,0,63,255,254,63,255,254,63,255,254,63,255,254,63,255,254]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,31,255,248,31,255,248,31,255,248,31,255,248,31,247,248,0,15,248,0,31,240,0,63,224,0,127,192,1,255,128,3,255,0,7,255,192,7,255,240,7,255,248,7,255,252,7,255,254,0,7,254,0,1,254,0,1,254,0,1,254,0,1,254,0,1,254,0,3,254,0,7,252,0,31,252,0,255,248,31,255,240,31,255,224,31,255,128,31,254,0,31,240,0]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,60,0,0,63,192,0,127,192,0,127,128,0,255,128,0,255,0,0,255,0,1,254,0,1,254,0,3,252,0,3,252,0,7,252,0,7,248,0,15,248,0,15,240,0,31,247,192,31,231,240,63,231,240,63,199,240,127,135,240,127,135,240,255,255,255,255,255,255,255,255,255,255,255,255,255,255,255,0,7,240,0,7,240,0,7,240,0,7,240,0,7,240,0,7,240]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,63,255,248,63,255,248,63,255,248,63,255,248,63,255,248,63,224,0,63,224,0,63,224,0,63,224,0,63,255,128,63,255,224,63,255,240,63,255,248,63,255,252,63,255,252,63,131,254,0,1,254,0,1,254,0,1,254,0,1,254,0,1,254,0,3,254,0,7,252,0,31,252,0,127,248,3,255,240,63,255,224,63,255,192,63,255,0,63,252,0,63,224,0]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,112,0,15,252,0,63,248,0,255,248,1,255,248,3,255,248,7,255,0,7,252,0,15,248,0,31,240,0,31,224,0,31,224,0,63,223,224,63,255,248,63,255,252,63,255,254,127,255,255,127,241,255,127,224,255,127,192,255,127,128,127,127,128,127,63,128,127,63,192,127,63,192,255,63,224,255,31,225,255,31,255,254,15,255,254,7,255,252,3,255,248,1,255,224]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,63,255,255,63,255,255,63,255,255,63,255,255,63,255,255,0,1,255,0,3,254,0,3,254,0,7,252,0,7,252,0,15,248,0,31,240,0,31,240,0,63,224,0,63,224,0,127,192,0,127,128,0,255,128,0,255,0,0,255,0,1,255,0,1,254,0,1,254,0,1,254,0,1,254,0,1,252,0,1,252,0,1,252,0,3,252,0,3,252,0,3,252,0]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,28,0,1,255,192,3,255,240,7,255,248,15,255,252,31,255,252,31,243,254,31,225,254,31,193,254,31,225,254,31,243,254,31,255,252,15,255,252,7,255,248,3,255,224,7,255,240,15,255,252,31,255,254,63,247,254,63,225,255,127,192,255,127,128,127,127,128,127,127,128,127,127,128,127,127,192,255,63,225,255,63,255,255,31,255,254,15,255,252,7,255,248,3,255,240]),24,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,28,0,1,255,192,7,255,240,15,255,248,31,255,252,63,255,252,63,227,254,127,193,254,127,128,255,127,128,255,127,128,255,127,128,255,127,128,255,127,128,255,127,193,255,63,231,255,63,255,255,31,255,255,15,255,255,7,255,255,1,254,254,0,1,254,0,1,254,0,3,252,0,7,252,0,31,248,0,127,240,7,255,224,7,255,192,3,255,128,3,254,0,3,248,0]),24,32,framebuf.MONO_HLSB)
]

# Heli non anime
# heli_side = framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,7,240,0,0,0,0,0,0,31,0,0,15,255,128,0,0,0,255,248,0,0,0,0,127,255,255,255,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,96,0,0,0,0,63,254,0,0,0,32,0,0,0,0,127,255,128,0,0,48,128,0,0,0,255,231,240,0,0,49,128,0,0,3,255,207,252,0,0,27,0,0,0,15,255,248,31,0,0,30,28,0,0,63,255,240,7,128,0,28,14,0,0,255,255,224,3,192,0,31,255,255,255,255,255,224,1,224,0,63,255,255,255,255,255,224,0,224,0,127,255,255,255,255,255,224,0,112,0,220,0,15,255,254,31,240,0,112,0,152,0,0,0,255,31,240,0,112,0,16,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB)

# Heli animed
heli_side_anime = [
    framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,7,240,0,0,0,0,0,0,63,0,0,15,255,128,0,0,0,255,224,0,0,0,0,127,255,255,255,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,32,0,0,0,0,31,254,0,0,0,32,0,0,0,0,127,255,128,0,0,48,128,0,0,0,255,231,240,0,0,49,128,0,0,3,255,207,252,0,0,27,0,0,0,15,255,248,31,0,0,30,28,0,0,63,255,240,7,128,0,28,14,0,0,255,255,224,3,192,0,31,255,255,255,255,255,224,1,224,0,63,255,255,255,255,255,224,0,224,0,111,255,255,255,255,255,224,0,112,0,220,0,15,255,254,31,240,0,112,0,152,0,0,0,255,31,240,0,112,0,16,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,0,3,255,0,0,0,0,7,240,0,0,0,1,255,0,0,31,252,0,0,0,0,0,1,255,255,240,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,32,0,0,0,0,31,254,0,0,0,32,0,0,0,0,127,255,128,0,0,48,0,0,0,0,255,231,240,0,0,48,0,0,0,3,255,207,252,0,0,24,0,0,0,15,255,248,31,0,0,24,60,0,0,63,255,240,7,128,0,31,238,0,0,255,255,224,3,192,0,255,255,255,255,255,255,224,1,224,0,143,255,255,255,255,255,224,0,224,0,15,255,255,255,255,255,224,0,112,0,28,0,15,255,254,31,240,0,112,0,24,0,0,0,255,31,240,0,112,0,16,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,7,224,0,0,3,224,0,0,0,0,0,63,128,1,254,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,32,0,0,0,0,31,254,0,0,0,32,0,0,0,0,127,255,128,0,0,176,0,0,0,0,255,231,240,0,0,240,0,0,0,3,255,207,252,0,0,120,0,0,0,15,255,248,31,0,0,56,28,0,0,63,255,240,7,128,0,28,14,0,0,255,255,224,3,192,0,15,255,255,255,255,255,224,1,224,0,15,255,255,255,255,255,224,0,224,0,15,255,255,255,255,255,224,0,112,0,29,128,15,255,254,31,240,0,112,0,24,128,0,0,255,31,240,0,112,0,16,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,240,1,240,0,0,0,0,0,0,0,30,31,0,0,0,0,0,0,0,0,3,240,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,32,0,0,0,0,31,254,0,0,0,36,0,0,0,0,127,255,128,0,0,52,0,0,0,0,255,231,240,0,0,60,0,0,0,3,255,207,252,0,0,24,0,0,0,15,255,248,31,0,0,24,28,0,0,63,255,240,7,128,0,28,14,0,0,255,255,224,3,192,0,31,255,255,255,255,255,224,1,224,0,31,255,255,255,255,255,224,0,224,0,31,255,255,255,255,255,224,0,112,0,60,0,15,255,254,31,240,0,112,0,56,0,0,0,255,31,240,0,112,0,48,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB),
    # framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,7,224,0,0,3,224,0,0,0,0,0,63,128,1,254,0,0,0,0,0,0,0,255,255,0,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,32,0,0,0,0,31,254,0,0,0,32,0,0,0,0,127,255,128,0,0,176,0,0,0,0,255,231,240,0,0,240,0,0,0,3,255,207,252,0,0,120,0,0,0,15,255,248,31,0,0,56,28,0,0,63,255,240,7,128,0,28,14,0,0,255,255,224,3,192,0,15,255,255,255,255,255,224,1,224,0,15,255,255,255,255,255,224,0,224,0,15,255,255,255,255,255,224,0,112,0,29,128,15,255,254,31,240,0,112,0,24,128,0,0,255,31,240,0,112,0,16,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB),
    framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,0,3,255,0,0,0,0,7,240,0,0,0,1,255,0,0,31,252,0,0,0,0,0,1,255,255,240,0,0,0,0,0,0,0,0,64,0,0,0,0,0,0,0,0,0,224,0,0,0,0,0,0,0,0,7,248,0,0,0,32,0,0,0,0,31,254,0,0,0,32,0,0,0,0,127,255,128,0,0,48,0,0,0,0,255,231,240,0,0,48,0,0,0,3,255,207,252,0,0,24,0,0,0,15,255,248,31,0,0,24,60,0,0,63,255,240,7,128,0,31,238,0,0,255,255,224,3,192,0,255,255,255,255,255,255,224,1,224,0,143,255,255,255,255,255,224,0,224,0,15,255,255,255,255,255,224,0,112,0,28,0,15,255,254,31,240,0,112,0,24,0,0,0,255,31,240,0,112,0,16,0,0,2,63,223,240,0,224,0,32,0,0,1,15,255,248,1,224,0,0,0,0,0,247,255,252,7,192,0,0,0,0,0,1,255,255,255,128,0,0,0,0,0,0,63,255,255,0,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,0,24,0,24,0,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,64,192,0,192,16,0,0,0,0,0,49,128,1,128,96,0,0,0,0,0,15,255,255,255,128,0,0,0,0,0,0,0,0,0,0,0]),80,32,framebuf.MONO_HLSB)
]

# Heli from top for Measurement screen
heli_top = framebuf.FrameBuffer(bytearray([0,0,0,0,0,0,0,0,0,0,0,0,0,31,255,255,255,224,0,0,0,0,1,128,1,128,0,0,0,0,0,0,192,0,192,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,24,0,16,0,0,0,0,0,0,15,255,252,0,0,0,0,0,0,56,0,6,0,0,0,0,0,1,192,0,1,128,0,0,0,0,6,0,0,0,64,0,24,0,0,8,0,0,0,32,0,8,0,0,48,0,0,0,16,0,8,0,0,192,0,0,0,16,31,255,255,255,0,0,0,0,8,32,0,0,0,0,0,0,0,8,31,255,255,252,0,0,0,0,8,4,8,0,7,0,0,0,0,8,127,200,0,0,192,0,0,0,16,0,24,0,0,48,0,0,0,16,0,0,0,0,8,0,0,0,32,0,0,0,0,6,0,0,0,64,0,0,0,0,1,192,0,1,128,0,0,0,0,0,57,0,6,0,0,0,0,0,0,31,255,252,0,0,0,0,0,0,24,0,16,0,0,0,0,0,0,48,0,48,0,0,0,0,0,0,96,0,96,0,0,0,0,0,0,192,0,192,0,0,0,0,0,1,128,1,128,0,0,0,0,0,31,255,255,255,224,0,0,0,0,0,0,0,0,0]),72,32,framebuf.MONO_HLSB)

# Depreciated
def affichePoids(poids):

    cen = int(poids/100)
    diz = int(math.fmod(poids, 100)/10)
    uni = int(math.fmod(poids, 10))

    di = int(math.fmod(poids*10, 10))
    ce = int(math.fmod(poids*100, 10))

    oled.blit(ch[cen], 0, 0)
    oled.blit(ch[diz], 24, 0)
    oled.blit(ch[uni], 48, 0)

    oled.pixel(72, 29, 1)
    oled.pixel(73, 29, 1)
    oled.pixel(72, 30, 1)
    oled.pixel(73, 30, 1)
    oled.pixel(72, 31, 1)
    oled.pixel(73, 31, 1)
    
    oled.blit(ch[di], 76,0)
    oled.blit(ch[ce], 100,0)

    oled.show()

def intro():    # Just for fun

    # Four Edges of the Display for brief visual control
    oled.pixel(0,0,1)
    oled.pixel(127,0,1)
    oled.pixel(127,63,1)
    oled.pixel(0,63,1)
    oled.show()
    sleep(0.5)
    oled.fill(0)

    # Athanaze et Diabolo
    oled.text('    Athanaze    ',0,15,1)
    oled.text('       &        ',0,25,1)
    oled.text('    Diabolo     ',0,35,1)
    oled.show()
    sleep(2)
    oled.fill(0)
      
    
    # Fly in - Animation
    for i in range(-80, 20):

        oled.blit(heli_side_anime[int(math.fmod(i,5))],i, int(22+i/2))
        oled.line(i,0,i,63,0)
        oled.show()
        sleep(0.0001)
    
    # Title - Animation
    oled.text('Weight & Balance',0,4,1)
    oled.text(' Power & more',0,14,1)
    oled.show()

    # cooldown - Animation
    for i in range(1, 10):
        oled.blit(heli_side_anime[int(math.fmod(i,5))],20, 32)
        oled.show()
        sleep(0.001)
    
    for i in range(1, 11):
        oled.blit(heli_side_anime[int(math.fmod(i,5))],20, 32)
        oled.show()
        sleep(i/40)

    # Stop rotor - Animation
    sleep(3)

    # Preheat - Animation
    for i in range(20, 1, -1):
        oled.blit(heli_side_anime[int(math.fmod(i,5))],20, 32)
        oled.show()
        sleep(i/30)

    for i in range(3, 40):
        oled.blit(heli_side_anime[int(math.fmod(i,5))],20, 32)
        oled.show()
        sleep(0.0001)

    oled.fill(0)

    # Fly - Animation
    for i in range(20, 128):

        oled.blit(heli_side_anime[int(math.fmod(i+1,5))],i,int(42-i/2))
        oled.line(i,0,i,63,0)
        oled.show()
        sleep(0.0001)
    
def measurement():      # main screen during measure of the weight and all param

    # Declaration
    global pd1
    global pd2
    global pd3
    global pd4

    global pdl
    global pdr
    global pdft
    global pdre
    global pdtot

    global posRac

    global collective
    global throttle
    
    # Read all weight sensor values 
    uart1 = UART(1, 9600)
    uart1.init(baudrate=9600, bits=8, parity=None, stop=1, timeout=80, rx=19)   # PIN 19 = Weight sensor 1 
    r = uart1.readline()
    if r != None:
        try:
            pd = float(r.decode("utf-8").replace('\r\n','').replace('=','')) # for hx711 with LED
            pd1 = int(pd*10)
            print('pd1 = '+pd1) 
        except:
            pass
    uart1.deinit()

    uart1 = UART(1, 9600)
    uart1.init(baudrate=9600, bits=8, parity=None, stop=1, timeout=80, rx=18)   # PIN 18 = Weight sensor 2 
    r = uart1.readline()
    if r != None:
        try:
            pd = float(r.decode("utf-8").replace('\r\n','').replace('=','')) # for hx711 with LED
            pd2 = int(pd*10)
            print('pd2 = '+pd2)  
        except:
            pass
    uart1.deinit()
    
    uart1 = UART(1, 9600)
    uart1.init(baudrate=9600, bits=8, parity=None, stop=1, timeout=80, rx=16)   # PIN 16 = Weight sensor 3 
    r = uart1.readline()        
    if r != None:
        try:
            pd = float(r.decode("utf-8").replace('\r\n','').replace('=','')) # for hx711 with LED
            pd3 = int(pd*10)
            print('pd3 = '+pd3)  
        except:
            pass
    uart1.deinit()

    uart1 = UART(1, 9600)
    uart1.init(baudrate=9600, bits=8, parity=None, stop=1, timeout=80, rx=17)   # PIN 17 = Weight sensor 4
    r = uart1.readline()            
    if r != None:
        try:
            pd = float(r.decode("utf-8").replace('\r\n','').replace('=','')) # for hx711 with LED
            pd4 = int(pd*10)
            print('pd4 = '+pd4)  
        except:
            pass
    uart1.deinit()

    # read collective and throttle

    collective = 0
    throttle = 0

    for i in range(1, 20) :
        collective += adcCollective.read()
        throttle += adcThrottle.read()
        # time.sleep(0.01)
    
    collective /= 20
    throttle /= 20

    print('Collective : '+str(collective))

    collective = round((collective-1690)/1690*90,0)
    if collective > 0 : collective *= 0.90
    posc = 8
    if collective < 0 : posc = 0
    if abs(collective) <=10: posc += 8
    
    throttle = round(throttle,0)


    pdtot = int(pd1+pd2+pd3+pd4)
    pdft = int(pd2+pd3)
    pdre = int(pd1+pd4)
    pdl = int(pd1+pd2)
    pdr = int(pd3+pd4)

    pdm1=0
    if pd1 < 0: pdm1 = 1
    pdm2=0
    if pd2 < 0: pdm2 = 1
    pdm3=0
    if pd3 < 0: pdm3 = 1
    pdm4=0
    if pd4 < 0: pdm4 = 1

    pdml=0
    if pdl < 0: pdml = 1
    pdmr=0
    if pdr < 0: pdmr = 1
    pdmft=0
    if pdft < 0: pdmft = 1
    pdmre=0
    if pdre < 0: pdmre = 1
    pdmtot=0
    if pdtot < 0: pdmtot = 1


    pos1 = 0
    if abs(pd1) <=10: pos1 += 8
    if abs(pd1) <=100: pos1 += 8
    if abs(pd1) <=1000: pos1 += 8
    pos2 = 0
    if abs(pd2) <=10: pos2 += 8
    if abs(pd2) <=100: pos2 += 8
    if abs(pd2) <=1000: pos2 += 8
    pos3 = 0
    if abs(pd3) <=10: pos3 += 8
    if abs(pd3) <=100: pos3 += 8
    if abs(pd3) <=1000: pos3 += 8
    pos4 = 0
    if abs(pd4) <=10: pos4 += 8
    if abs(pd4) <=100: pos4 += 8
    if abs(pd4) <=1000: pos4 += 8

    posl = 0
    if abs(pdl) <=10: posl += 8
    if abs(pdl) <=100: posl += 8
    if abs(pdl) <=1000: posl += 8
    posr = 0
    if abs(pdr) <=10: posr += 8
    if abs(pdr) <=100: posr += 8
    if abs(pdr) <=1000: posr += 8

    posft = 0
    if abs(pdft) <=10: posft += 8
    if abs(pdft) <=100: posft += 8
    if abs(pdft) <=1000: posft += 8

    posre = 0
    if abs(pdre) <=10: posre += 8
    if abs(pdre) <=100: posre += 8
    if abs(pdre) <=1000: posre += 8

    postot = 0
    if abs(pdtot) <=10: postot += 8
    if abs(pdtot) <=100: postot += 8
    if abs(pdtot) <=1000: postot += 8

    # Heli in the middle
    oled.fill(0)
    oled.blit(heli_top,54,16)
    oled.text(str(int(round(collective,0))),90+posc,29)
    oled.pixel(115,27,1)
    oled.pixel(114,28,1)
    oled.pixel(116,28,1)
    oled.pixel(115,29,1)

    # Separation line vertical
    #oled.line(42,0,42,63,1)

    # display weight on screen
    oled.text(str(abs(pd1)),48+pos1,0)
    oled.text(str(abs(pd2)),92+pos2,0)
    oled.text(str(abs(pd3)),92+pos3,56)
    oled.text(str(abs(pd4)),48+pos4,56)

    # display small decimal to save place and underline if minus
    oled.pixel(47+24,7,1)
    oled.pixel(47+24,8,1)
    if pdm1 == 1 : oled.line(47-2+pos1,3,47+pos1,3,1)
    
    oled.pixel(91+24,7,1)
    oled.pixel(91+24,8,1)
    if pdm2 == 1 : oled.line(91-2+pos2,3,91+pos2,3,1)

    oled.pixel(91+24,63,1)
    oled.pixel(91+24,62,1)
    if pdm3 == 1 : oled.line(91-2+pos3,59,91+pos3,59,1)

    oled.pixel(47+24,63,1)
    oled.pixel(47+24,62,1)
    if pdm4 == 1 : oled.line(47-2+pos4,59,47+pos4,59,1)

    oled.text('Frnt',2,3)
    oled.text(str(abs(pdft)),2+posft,13)
    oled.pixel(25,20,1)
    oled.pixel(25,21,1)
    if pdmft == 1 : oled.line(0+posft,16,1+posft,16,1)

    oled.text('Rear',2,25)
    oled.text(str(abs(pdre)),2+posre,35)
    oled.pixel(25,42,1)
    oled.pixel(25,43,1)
    if pdmre == 1 : oled.line(0+posre,38,1+posre,38,1)

    # Horizontal line sum
    oled.line(0,46,38,46,1)

    oled.text(str(abs(pdtot)),2+postot,50)
    oled.pixel(25,57,1)
    oled.pixel(25,58,1)
    if pdmtot == 1 : oled.line(0+postot,52,1+postot,52,1)

    oled.line(0,60,38,60,1)
    oled.line(0,62,38,62,1)

    # Left & Right sum
    oled.text(str(abs(pdl)),54+posl,17)
    oled.pixel(53+24,24,1)
    oled.pixel(53+24,25,1)
    if pdml == 1 : oled.line(54-3+posl,20,53+posl,20,1)
    
    oled.text(str(abs(pdr)),54+posr,40)
    oled.pixel(53+24,47,1)
    oled.pixel(53+24,48,1)
    if pdmr == 1 : oled.line(54-3+posr,43,53+posr,43,1)

    # Anime RAC - to visualy check if running
    posRac += 1
    posNow = abs(math.fmod(posRac,7)-4)

    for i in range(0,posNow):
        oled.pixel(55+i, 34, 0)
        oled.pixel(63-i, 34, 0)
  

    # recordLetter
    oled.text(recordLetter,41,17,1)
    
    oled.show()
    
def printTicket():

    print("printTicket is running ...")
    
    printer = Adafruit_Thermal(pins=(Pin(XX),), heatdots=255, heattime=255, heatinterval=255)  # PIN ?? not defined yet
    printer.setDefault()
    
    # Print : Title
    printer.doubleHeightOn()
    printer.boldOn()
    printer.println("Weight & Balance")
    printer.setDefault()

    # print : Basic weights
    printer.boldOn()
    printer.println("Rear Left : "+str(pd1/10))
    printer.println("Rear Right : "+str(pd4/10))
    
    printer.println("Total Rear : "+str(pdre/10))
   
    printer.println("Front Left : "+str(pd2/10))
    printer.println("Front Right : "+str(pd3/10))

    printer.println("Total Front : "+str(pdft/10))
    printer.println("Total Left : "+str(pdl/10))
    printer.println("Total Right : "+str(pdr/10))
          
    printer.println("Total : "+str(pdtot/10))

    printer.feed()
    printer.feed()
    printer.feed()




# Main Main Main Main

# intro()

# Main loop ###########################################################
while True:

    # fixing time
    dt = ds.formattedDateTimeStr()
    dtbis = ds.formattedDateTimeStrBis()
    tt = time.ticks_ms()
 
    print ('measurement')
    measurement()
    print('button 1 : '+str(button1.value()))
    print('button 2 : '+str(button2.value()))

    # Detect if button1 is pressed :-> Print
    if button1.value() == 0:

        print('Button 1 : Delete data fîle')
        # printTicket()
        f = open('data.txt','w')
        f.write('recordLetter,time,ticks,RL,FL,FR,RR,collective,throttle\n')
        f.close()
        lastRecordLetter = '@'
        recordLetter = 'A'
        f = open('recordLetter.txt','w')
        f.write(lastRecordLetter)
        f.close()
 
        
    # Detect if button2 is pressed :-> Record
    if button2.value() == 1:    # not pressed

        if recordOn == True:    # End of recording
            
            f = open('recordLetter.txt','w')
            f.write(recordLetter)
            f.close()
            lastRecordLetter = recordLetter
            n = ord(lastRecordLetter)+1
            if n >90 : n = 65
            recordLetter = chr(n)
            
        recordOn = False
  
    
    if button2.value() == 0:    # pressed

        print('Saving in data.txt : '+str(time.ticks_ms()))
    
        if recordOn == False :  # Begin of recording

             recordOn = True

        f = open('data.txt','a')
        f.write(recordLetter+','+dt+','+str(tt)+','+str(pd1)+','+str(pd2)+','+str(pd3)+','+str(pd4)+','+str(int(collective))+','+str(int(throttle))+'\n')
        f.close()

        ICON = [
            [ 1, 1, 1, 1, 1, 1, 1, 0, 0],
            [ 1, 0, 1, 0, 0, 0, 1, 1, 0],
            [ 1, 0, 1, 1, 1, 1, 1, 0, 1],
            [ 1, 0, 0, 0, 0, 0, 0, 0, 1],
            [ 1, 0, 1, 1, 1, 1, 1, 0, 1],
            [ 1, 0, 1, 0, 0, 0, 1, 0, 1],
            [ 1, 0, 1, 0, 0, 0, 1, 0, 1],
            [ 1, 0, 1, 0, 0, 0, 1, 0, 1],
            [ 1, 1, 1, 1, 1, 1, 1, 1, 1],
        ]

        for y, row in enumerate(ICON):
            for x, c in enumerate(row):
                oled.pixel(41+x, 27+y, c)    

        # oled.text(recordLetter,41,17)

        oled.show()

    # Detect if a webpage is requested
    print('Calling web_service')
    
    if web_service() == True:

        ICON = [
            [ 0, 0, 1, 1, 1, 1, 1, 0, 0],
            [ 0, 1, 0, 0, 0, 0, 0, 1, 0],
            [ 1, 0, 0, 0, 0, 0, 0, 0, 1],
            [ 0, 0, 1, 1, 1, 1, 1, 0, 0],
            [ 0, 1, 0, 0, 0, 0, 0, 1, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0, 0],
            [ 0, 0, 0, 1, 1, 1, 0, 0, 0],
            [ 0, 0, 1, 0, 0, 0, 1, 0, 0],
            [ 0, 0, 0, 0, 0, 0, 0, 0, 0],
        ]

        for y, row in enumerate(ICON):
            for x, c in enumerate(row):
                oled.pixel(41+x, 39+y, c)    

        oled.show()
 

    # sleep(0.1)


